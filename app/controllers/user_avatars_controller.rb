class UserAvatarsController < ApplicationController
  before_action :authenticate_user!

  def new
  end

  def create
    current_user.update(user_params)

    redirect_to new_user_avatar_path,
      notice: 'Avatar was successfully set.'
  end

  private

  def user_params
    params.require(:user).permit(:avatar)
  end
end
